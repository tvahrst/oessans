<!DOCTYPE html>
<html lang="de">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="Partyservice Münster, Catering Münster, türkischer Partyservice, mediteranes Catering" />
  <meta name="google-site-verification" content="T6Eh8gOuhH3BejiO-ZtqO6zz7XcOH_dkqaulFtJM-BU" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta name="msvalidate.01" content="7245704B8090AA19C4E2C09640FC1CE1" />
  <link rel="shortcut icon" type="image/x-icon" href="favicon.png">
  <link href='http://fonts.googleapis.com/css?family=Julius+Sans+One|Esteban' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
	<?php
		require "header.php";
		require "script.php";
	?>
<meta name="description" content="Össan´s Partyservice Münster verwöhnt Ihre Gäste auf Hochzeiten, Firmenevents und privaten Feiern mit mediterranen Köstlichkeiten und türkischen Spezialitäten. In Münster und dem Münsterland.">
<title>Wissenswertes zu Össan's Partyservice Münster</title>
	</head>
	<body>
	<?php
		require "nav.php";
	?>
<section id="main" class="pearlon" style="z-index:0;" style="height:auto;">
	<article id="feiern" style="height:auto;">
		<section id="eighth" data-offsety="0" data-speed="18" data-type="background" style="height:2000px;">    	
		<article id="container_feiern">
		<div class="gallery" data-type="video" data-offsetY="0" data-speed="2">
							
			</div>
			<h2 class="heading">FAQ</h2>
			<div class="content faq">
			<h1 style="font-size:30px;">Wissenswertes zu Össans Partyservice Münster</h1>
				<h3 class="trigger trigger_active"><span class="pfeil" style="float:left;"></span>Wohin liefern Sie und was kostet die Lieferung?</h3>
						<div class="card toggle_container" style="display: block;">
							<p class="p-content">
							Wir bereiten alle Speisen frisch zu und möchten Ihnen die bestmögliche Qualität liefern. Um unserem Qualitätsanspruch gerecht zu werden, haben wir das Liefergebiet eingegrenzt. Aber seien Sie unbesorgt. Össan's Partyservice beliefert Sie im gesamten Münsterland und über die Grenzen hinaus. Werfen Sie doch einen Blick auf unser Liefergebiet im <a href="/#footer" title="Össan's Partyservice Kontakt">Kontaktbereich</a>. Bei Anfragen, welche nicht durch das Liefergebiet abgedeckt werden, kontaktieren Sie uns bitte vorab um gemeinsam nach einer Lösung zu suchen.
							</p>
							<p class="p-content">
							Die Lieferung erfolgt ansonsten unabhängig vom Auftragsvolumen kostenlos. Dasselbe gilt für die Abholung des Geschirrs etc..
							</p>
						</div>					
				<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Liefern Sie auch Geschirr und Besteck und was kostet das?</h3>
						<div class="card toggle_container">
								<p class="p-content">
							Selbstverständlich liefern wir Ihnen ausreichend Geschirr und Besteck um alle Speisen gebührend zu genießen. Wir berechnen für diesen Service keinen Aufpreis!
							</p>
						</div>	
				<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Wie und wann erfolgt die Zahlung?</h3>
						<div class="card toggle_container">
								<p class="p-content">
							Die Zahlung wird nach erfolgreicher Lieferung durchgeführt und ist wahlweise in Bar oder per Banküberweisung zu entrichten. Sie erhalten bei der Lieferung oder per Email eine Rechnung mit ausgewiesener Umsatzsteuer.
							</p>
						</div>		
				<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Wann muss ich spätestens meine Bestellung aufgeben?</h3>
						<div class="card toggle_container">
								<p class="p-content">
							Wie so oft gilt auch bei Össan's Partyservice: Je früher desto besser. Als Faustregel gilt daher: Kontaktieren Sie uns sobald Sie den Termin für Ihre Veranstaltung fixiert haben. Dies gilt insbesondere für größere Gesellschaften. Die frühzeitige Planung liegt dabei nicht nur uns am Herzen, sondern ist auch für Sie wichtig um größtmögliche Planungssicherheit zu erlangen. Wir wachsen stetig und schnell und daher kann es bei sehr kurzfristigen Anfragen passieren, dass unsere Kapazitäten für Ihren Wunschtermin bereits ausgeschöpft sind.
							</p>
						</div>	
				<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Ich möchte gerne meine erste Bestellung aufgeben. Gibt es Referenzen um mir einen Eindruck zu verschaffen?</h3>
						<div class="card toggle_container">
								<p class="p-content">
							Selbstverständlich hat Össan's Partyservice schon viele Gaumen verzückt. Im Zuge des Relaunches von <a href="/" title="Össan's Partyservice">unserer Webpräsenz</a> arbeiten wir auch an einem neuen Referenzsystem um die Stimmen unserer Kunden einzufangen und Ihnen eine transparentes Bild aus Kundenperspektive zu bieten.
							</p>
						</div>	
				<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Ich habe noch nie bei Össan's Partyservice bestellt. Wie läuft der Bestellprozess ab?</h3>
						<div class="card toggle_container">
								<p class="p-content">
							Bei uns wird Persönlichkeit und Individualität groß geschrieben. Wir haben uns daher absichtlich dagegen entschieden, Ihnen ein Angebot von der Stange anzubieten. Wenn Sie sich einen guten Überblick über unser Angebot verschafft haben, haben Sie 3 Möglichkeiten uns zu kontaktieren:
							<p class="p-content">1. Rufen Sie uns an unter 02506-6764 oder 0160-8495685</p>
							<p class="p-content">2. Schreiben Sie uns eine Email über das <a href="/#footer" title="Össan's Partyservice Kontakt">Kontaktformular</a></p>
							<p class="p-content">3. Schreiben Sie uns direkt eine E-Mail an <a href="mailto:info@oessans.de?subject=Meine%20Anfrage%20bei%20Össan's%20Partyservice.de" title="Email schicken an Össan's Partyservice">info@oessans.de</a>. Vergessen Sie dabei bitte nicht die relevanten Angaben wie Datum, Personenanzahl, Anlass und Lieferort mit anzugeben und hinterlassen Sie eine Rufnummer. Sofern Sie bereits Wünsche oder Ideen haben wie ihre Zusammenstellung aussehen könnte, geben Sie dies ebenfalls an.
							<p class="p-content">Nachdem Sie uns über einen der oberen Wege kontaktiert haben, werden wir mit Ihnen ein Termin vereinbaren um die Zusammenstellung der Speisen und den organisatorischen Ablauf mit Ihnen abzustimmen. Anschließend werden wir auf dieser Grundlage ein individualisiertes Angebot zukommen lassen. Gerne laden wir Sie auch ein um uns persönlich kennen zu lernen. Nachdem Sie das Angebot angenommen haben, übernehmen wir die restliche Arbeit und Sie können Ihrer Veranstaltung entgegenfiebern</p>
							<p class="p-content">
							Wenn Sie uns, unsere Arbeitsweise und Motivation besser kennenlernen möchten, schauen Sie doch mal was wir über unsere <a href="/philosophie" title="Össan's Partyservice Philosophie">Philosophie</a> zu sagen haben. 
							</p>
								</p>
						</div>				
			</div>		
	    </article>
	</article>
</section>

</body>
</html>
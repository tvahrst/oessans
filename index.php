	<?php
		require "contact.php";
		require "header.php";
		require "script.php";
	?>
<meta name="description" content="Össan´s Partyservice Münster verwöhnt Ihre Gäste auf Hochzeiten, Firmenevents und privaten Feiern mit mediterranen Köstlichkeiten und türkischen Spezialitäten. In Münster und dem Münsterland.">
<title>Össan´s: Partyservice Münster - Türkisches Catering und mediterrane Buffets</title>
	</head>
	<body>
	<!-- Preloader -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>

	<?php
		require "nav.php";
	?>
<section id="main" class="pearlon" style="z-index:0;">
	<!--<div id="rightcol">
	</div>-->
	<nav id="sidenav">
		<ul class="bmenu onnav">
			<li class="current"><a href="#feiern">Feste</a></li>
			<li><a href="#fingerfood">Fingerfood</a></li>	
			<li><a href="#vorspeisen">Vorspeisen</a></li>
			<li><a href="#hauptspeisen">Hauptgerichte</a></li>
			<li><a href="#desserts">Desserts</a></li>
			<li><a href="#contact">Kontakt</a></li>
		</ul>
	</nav>
	
	<article id="feiern">
		<section id="first" data-type="background">
			<?php
				require "feiern.php";
			?>
		</section>
	</article>
	<article id="fingerfood">
		<section id="fifth" data-type="background">
			<?php
				require "fingerfood.php";
			?>
		</section>
	</article>
	<article id="vorspeisen">
		<section id="second" data-type="background">
			<?php
				require "vorspeisen.php";
			?>
		</section>
	</article>
	<article id="hauptspeisen">
		<section id="third" data-type="background">
			<?php
				require "hauptspeisen.php";
			?>
		</section>
	</article>
	<article id="desserts">
		<section id="fourth" data-type="background">
			<?php
				require "dessert.php";
			?>
		</section>
	</article>
	<article id="contact">
		<section id="sixth" data-offsety="700" data-speed="8" data-type="background" style="border:none;">    	
			<article id="container_contact">
				<h2 class="heading">Informationen</h2>
				<div class="content">
				<h6>Preise mengenabhängig und auf Anfrage</h6>
				<p>
				Bitte kontaktieren Sie uns. Wir beraten Sie gerne und
				unterbreiten Ihnen ein individuelles Angebot.
				</p>
				<h6 style="text-align:center;">Guten Appetit & Afiyet olsun!</h6>
				<div id="anfrage"><a href="#footer"><img src="images/anfrage.png" alt="Anfrage für Partyservice stellen" style="text-align:center; position:relative; margin:30px auto 10px auto; display:block;"/></a>
				</div>
				</div>
			</article>
		</section>
	</article>
	<footer id="footer">
	<span id="contact_head">Kontakt</span>
		<div id="foot1">
		<a href="https://mapsengine.google.com/map/edit?mid=zGCTu1T-wqQk.kbvwB-7coKBM" target="_blank" style="border: 0 none;"><img src="images/map.jpg" width="370" height="370" style="padding:15px" alt="Liefergebiet von Össan" class="mapimg"></a>
		</div>
			<div id="foot3">
			<ul id="address">
				<li>Özhan Göcmen-Lütkenhöner</li>
				<li>Tönne-Vormann-Weg 42</li>
				<li>48167 Münster</li>
				<li style="margin-top:14px;">Fon: 02506 - 6764</li>
				<li>Mobil: 0160 - 8495685</li>
				<li>Fax: 02506 - 3039396</li>
				<li>info@oessans.de</li>
			</ul>
			</div>
		<div id="foot2">
			<div id="contact2">
				<form action="" method="post" id="formID">
					<fieldset>
						<dl>
							<dt><label for="Name"></label></dt>
							<dd><input type="text" name="Name" id="Name" placeholder="Name *" class="validate[required] text-input" value="<?php echo (isset($_POST['Name']) && $_POST['Name'] != '' ? $_POST['Vorname'] : ''); ?>" /></dd>
								
							<dt><label for="Anlass"></label></dt>
							<dd><input type="text" name="Anlass" id="Anlass" placeholder="Anlass" value="<?php echo (isset($_POST['Anlass']) && $_POST['Anlass'] != '' ? $_POST['Anlass'] : ''); ?>" /></dd>
								
							<dt class="clearleft"><label for="Firma"></label></dt>
							<dd><input type="text" name="Firma" id="Firma" placeholder="Firma" value="<?php echo (isset($_POST['Firma']) && $_POST['Firma'] != '' ? $_POST['Firma'] : ''); ?>" /></dd>
								
							<dt><label for="Datum"></label></dt>
							<dd><input type="text" name="Datum" id="Datum" placeholder="Datum *" class="validate[required] text-input" value="<?php echo (isset($_POST['Datum']) && $_POST['Datum'] != '' ? $_POST['Datum'] : ''); ?>" /></dd>
																
							<dt class="clearleft"><label for="EMail"></label></dt>
							<dd><input type="text" name="EMail" id="EMail" placeholder="E-Mail *" class="validate[required,custom[email]]" value="<?php echo (isset($_POST['EMail']) && $_POST['EMail'] != '' ? $_POST['EMail'] : ''); ?>" /></dd>
								
							<dt><label for="Personen"></label></dt>
							<dd><input type="text" name="Personen" id="Personen" placeholder="Personenanzahl *" class="validate[required] text-input" value="<?php echo (isset($_POST['Personen']) && $_POST['Personen'] != '' ? $_POST['Personen'] : ''); ?>" /></dd>
																	
							<dt class="clearleft"><label for="Telefon"></label></dt>
							<dd><input type="text" name="Telefon" id="Telefon" placeholder="Festnetz oder Mobil *" value="<?php echo (isset($_POST['Telefon']) && $_POST['Telefon'] != '' ? $_POST['Telefon'] : ''); ?>" /></dd>
							
							<dt><label for="Lieferort"></label></dt>
							<dd><input type="text" name="Lieferort" id="Lieferort" placeholder="Lieferort *" class="validate[required] text-input" value="<?php echo (isset($_POST['Lieferort']) && $_POST['Lieferort'] != '' ? $_POST['Lieferort'] : ''); ?>" /></dd>
											
							<dt class="clearleft"><label for="Bemerkungen"></label></dt>
							<dd><textarea rows="12" class="conttext" id="Bemerkungen" name="Bemerkungen" placeholder="Bitte schreiben Sie uns Ihr Anliegen"><?php echo (isset($_POST['Bemerkungen']) && $_POST['Bemerkungen'] != '' ? $_POST['Bemerkungen'] : ''); ?></textarea></dd>
						</dl>
						<p id="sendfield">
							<input type="submit" value="Senden" class="sendfields" />
						</p>
					</fieldset>
				</form>
			</div>
			</div>
	</footer>
</section>
<!-- Preloader -->
<script type="text/javascript">
        $(window).load(function() { 
            $('#status').fadeOut(); 
            $('#preloader').delay(350).fadeOut('slow');
        })
</script>
</body>
</html>
	<?php
		require "header.php";
		require "script.php";
	?>
<meta name="description" content="Össan´s Partyservice Münster verwöhnt Ihre Gäste auf Hochzeiten, Firmenevents und privaten Feiern mit mediterranen Köstlichkeiten und türkischen Spezialitäten. In Münster und dem Münsterland.">
<title>Die Philosophie der mediterranen und türkischen Küche</title>
	</head>
	<body>
	<?php
		require "nav.php";
	?>
<section id="main" class="pearlon" style="z-index:0;" style="height:auto;">
	<article id="feiern" style="height:auto;">
		<section id="seventh" data-offsety="0" data-speed="18" data-type="background" style="height:2000px;">    	
		<article id="container_feiern">
		<div class="gallery" data-type="video" data-offsetY="0" data-speed="2">
								<h1 style="font-size:25px; margin-top:5px;">Unsere Partner</h1>
								<a href="http://www.tanzschule-victor.de/"  title="Tanzschule Victor" target="_blank"><div class="view"><img src="images/tanzschule.png" alt="Tanzschule Victor" /><div class="mask"><h4>Tanzschule Victor - Tanzschule in Münster Wolbeck</h4></div></div></a>
								<a href="http://www.weinamschloss.de/"  title="Weingut am Schloss" target="_blank"><div class="view"><img src="images/wein.png" alt="Weingut am Schloss" /><div class="mask"><h4>Weingut am Schloss - Weinhandlung in Münster Wolbeck</h4></div></div></a>
					
			</div>
			<h2 class="heading">philosophie</h2>
			<div class="content">
				<h6>Mediterrane und südländische Lebensweise erleben</h6>
				<p style="font-size: 15px !important;">
					Schätzen Sie die südländische Küche? Haben Sie schon einmal türkisch gegessen? Vielleicht im letzten Türkei- Urlaub, in einem Restaurant oder auf einer Party bei Freunden?
					Anknüpfend an meine mehrjährige Erfahrung im türkischen Restaurant "Merhaba" in Münster möchte ich Ihnen meinen Partyservice Össans präsentieren. Seit 1993 ist er bei Freunden der türkisch- mediterranen Küche unter dem Namen "Ägäis" bekannt. Seit 2010 habe ich mich zu einer Namensänderung entschlossen, um der Individualität meines Partyservices mehr Ausdruck zu verleihen.
					Mit der Vielfalt an warmen und kalten Vorspeisen, den interessanten Hauptgerichten und den köstlichen Desserts bietet die türkisch- mediterrane Küche ideale Vorraussetzungen für verschiedene Anlässe, z.B. Büffets für Geburtstags- und Hochzeitsfeiern, Empfänge mit Fingerfood. Bei einem persönlichen Gespräch gestalte ich mit Ihnen zusammen Ihr individuelles Büffet. Auch eine Döner- oder Grillparty könnte ein Highlight für Ihre Gäste sein. Nicht nur für Feierlichkeiten in großem Rahmen, auch für Ihre private kleine Feier zuhause bin ich Ihr richtiger Ansprechpartner.
					Neben meinem Partyservice und meinen privaten Kochkursen, die sich seit mehreren Jahren großer Beliebtheit bei Jung und Alt erfreuen, würde ich auch gerne für Sie und Ihre Freunde an Ihrem heimischen Herd meine kreative Küche präsentieren.
					Als zusätzlichen Service könnte ich Ihnen entsprechende Räumlichkeiten für Ihre Feier empfehlen, auf Wunsch auch mit Bedienung, türkischen Weinen und Bauchtanz.
					</br></br>Ich freue mich auf Sie!
					</br>Ihr Özhan Göcmen-Lütkenhöner 
				</p>
				<img src="images/sonstige/DSC_5633.jpg" alt="Özhan Göcmen-Lütkenhöner" style="margin-top:30px; max-width: 100%;" />
			</div>			
	    </article>
	</article>
</section>

</body>
</html>
		<article id="container_feiern" >
			<div class="gallery" data-type="video" data-offsetY="0" data-speed="2">
					<?php
						$Bilder = array();
						$Ordner = 'images/feste';
						$dateiendungen = array('png', 'jpg');
						$anzahl = 40;
						$nummern = array();
						
						$ordner = opendir($Ordner);
						while ($Datei = readdir($ordner)) {
							if(!is_dir($Datei)) {
								if ($Datei != '..') {
									if (strstr($Datei, '.')) {
										$punkt = strrpos($Datei, '.');
										$endung = strtolower(substr($Datei, $punkt + 1));
										
										if (in_array($endung, $dateiendungen)) {
											$Bilder[] = $Ordner . '/' . $Datei;
										}
									} 
								}
							}
						}
						closedir($ordner);
						
						$anzahlbilder = count($Bilder) - 1;
						if ($anzahl > $anzahlbilder) {
							$anzahl = $anzahlbilder;
						}
						
						for ($i = 0; $i <= $anzahl; $i++) {
							srand(microtime()*1000000);
							$nummer = rand(0, $anzahlbilder);
							$path_parts = pathinfo($Bilder[$nummer]);
							if (!in_array($nummer, $nummern)) {
								$nummern[] = $nummer;
								echo '<a href="' . $Bilder[$nummer] . '" class="photobox" rel="help" title="' . $path_parts["filename"] . '"><div class="view"><img src="' . $Bilder[$nummer] . '" alt="' . $path_parts["filename"] . '" /><div class="mask"><h4>' . $path_parts["filename"] . '</h4></div></div></a>';
							} else {
								$i--;
							}
						}
						
					?>				
			</div>
			<h2 class="heading">feste feiern</h2>
				<div class="content">
				<h1>Össan´s Partyservice</h1>
				<h6>Türkisch. Mediterran. International</h6>
				<p>
				Sie sind auf der Suche nach einem einzigartigen kulinarischen Erlebnis für Ihre nächste Feier? Wir verwöhnen Sie und Ihre Gäste mit mediterranen 
				Köstlichkeiten und türkischen Spezialitäten auf Hochzeiten, Geburtstagen oder Firmenfeiern. Auch gerne verköstigen wir Sie in kleinem Rahmen bei Ihnen zu Hause. 
				Alles ganz nach Ihren Wünschen.
				</p>
				<h6>Catering für Firmenevents:</h6>
				<p>
				Ob Kundenevent, Weihnachtsfeier oder Sommerfest, wir begeistern Ihre Kunden und Mitarbeiter mit einem außergewöhnlichen 
				Buffet von Spezialitäten des Südens – individuell und passend zusammen gestellt für jeden Anlass – ganz nach Ihren Wünschen.
				</p>
				<h6>Perfekter Partyservice für Hochzeiten und private Feste:</h6>
				<p>
				Sie haben ein privates Fest, in kleinem oder großen Rahmen und möchten Ihre Gäste mit einer außergewöhnlichen Auswahl an mediterranen 
				Spezialitäten verwöhnen, dann sind wir der richtige Ansprechpartner – wir stellen ein Menü mit Köstlichkeiten des Südens und der Türkei zusammen, 
				passend für Ihren Anlass und individuell auf Sie abgestimmt.
				</p>
				</div>
	    </article>
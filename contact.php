<?php
$zieladresse = 'info@oessans.de';
$absendername = 'Oessans Partyservice';
$betreff = 'Anfrage an Partyservice';
$urlDankeSeite = 'danke.php';
$trenner = ":\t"; // Doppelpunkt + Tabulator
$message = null;

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	foreach ($_POST as $key => $value) {
		$_POST[$key] = stripslashes(trim($value));
	}
	
	if ($_POST['Name'] == '' || $_POST['EMail'] == '' || $_POST['Datum'] == '' ||  $_POST['Telefon'] == '' ||  $_POST['Personen'] == '') {
		$message = 'Bitte füllen Sie alle mit * markierten Pflichfelder aus.';
	} else {
		$header = array();
		$header[] = "From: ".mb_encode_mimeheader($absendername, "utf-8", "Q")." <".$absenderadresse.">";
		$header[] = "MIME-Version: 1.0";
		$header[] = "Content-type: text/plain; charset=utf-8";
		$header[] = "Content-transfer-encoding: 8bit";
		
		$mailtext = "";

		foreach ($_POST as $name => $wert) {
			if (is_array($wert)) {
				foreach ($wert as $einzelwert) {
					$mailtext .= $name.$trenner.$einzelwert."\n";
				}
			} else {
				$mailtext .= $name.$trenner.$wert."\n";
			}
		}
		
		mail(
			$zieladresse, 
			mb_encode_mimeheader($betreff, "utf-8", "Q"), 
			$mailtext,
			implode("\n", $header)
		) or die("Die Mail konnte nicht versendet werden.");
		
		header("Location: $urlDankeSeite");
		exit;
	}
}

header("Content-type: text/html; charset=utf-8");

?>
		<article id="container_hauptspeisen">
			<div class="gallery" data-type="video" data-offsetY="900" data-speed="4">
					<?php
						$Bilder = array();
						$Ordner = 'images/main';
						$dateiendungen = array('png', 'jpg');
						$anzahl = 40;
						$nummern = array();
						
						$ordner = opendir($Ordner);
						while ($Datei = readdir($ordner)) {
							if(!is_dir($Datei)) {
								if ($Datei != '..') {
									if (strstr($Datei, '.')) {
										$punkt = strrpos($Datei, '.');
										$endung = strtolower(substr($Datei, $punkt + 1));
										
										if (in_array($endung, $dateiendungen)) {
											$Bilder[] = $Ordner . '/' . $Datei;
										}
									} 
								}
							}
						}
						closedir($ordner);
						
						$anzahlbilder = count($Bilder) - 1;
						if ($anzahl > $anzahlbilder) {
							$anzahl = $anzahlbilder;
						}
						
						for ($i = 0; $i <= $anzahl; $i++) {
							srand(microtime()*1000000);
							$nummer = rand(0, $anzahlbilder);
							$path_parts = pathinfo($Bilder[$nummer]);
							if (!in_array($nummer, $nummern)) {
								$nummern[] = $nummer;
								echo '<a href="' . $Bilder[$nummer] . '" class="photobox" rel="tag" title="' . $path_parts["filename"] . '"><div class="view"><img src="' . $Bilder[$nummer] . '" alt="' . $path_parts["filename"] . '" /><div class="mask"><h4>' . $path_parts["filename"] . '</h4></div></div></a>';
							} else {
								$i--;
							}
						}
						
					?>					
			</div>
			<h2 class="heading">hauptgerichte</h2>
			<div class="content">				
					<p>
					Kebap, Lamm, Köfte, Spieße – mediterrane Köstlichkeiten für den großen Hunger zum Hauptgang. Wir haben eine riesige Auswahl an türkischen und südländischen 
					Hauptgängen, mit Fleisch oder Fisch, aber natürlich bieten wir auch eine reichhaltige Auswahl vegetarischer und auch veganer Hauptgerichte an – 
					auf jeden Ihrer Wünsche gehen wir dabei gerne ein.
					</p>
					<p>
					Ob Sommer, Frühling, Herbst oder Winter. Zu jeder Jahreszeit stellen wir für Sie die perfekte Auswahl an Hauptgerichten zusammen: Frisch gegrillte Fleisch- oder Fischgerichte im 
					Sommer zur Gartenparty oder ein perfektes Menü zur Hochzeit oder Firmenfeier. Wir erarbeiten mit Ihnen gemeinsam ein Menü, das Ihre Gäste überrascht und begeistert.
					</p>
				<h3 class="trigger3 trigger_active3"><span class="pfeil" style="float:left;"></span>Topf- und Pfannengerichte</h3>
						<div class="card toggle_container3" style="display: block;">
							<p class="p-content">
							Zu folgenden Gerichten empfehlen wir als Beilage türkischen Reis oder Bulgur-Ratatoille.
							</p>	
							<ul>
								<li><h5>Tas Kebabi</h5><p>Lammragout</p></li>
								<li><h5>Etli Türlü</h5><p>Lamm-Gemüsetopf</p></li>
								<li><h5>Überbackenes Lammragout</h5><p>mit Auberginen und Tomaten</p></li>
								<li><h5>Karniyarik</h5><p>gefüllte Auberginen mit Hackfleisch</p></li>
								<li><h5>Moussaka</h5><p>Hackfleisch-Gemüse-Auflauf</p></li>
								<li><h5>Hähnchen und- Putendöner aus der Pfanne</h5><p></p></li>
								<li><h5>Hähnchen-Reis-Gemüsepfanne</h5><p></p></li>
								<li><h5>Gyrospfanne</h5><p></p></li>
								<li><h5>Vegetarischer Gemüseauflauf</h5><p>mit Schafkäse</p></li>
						</ul>
						</div>
				<h3 class="trigger3"><span class="pfeil" style="float:left;"></span>Fleischgerichte</h3>
						<div class="card toggle_container3">
							<p class="p-content">
							Zu folgenden Fleischgerichten empfehlen wir wahlweise als Beilage türkischen Reis, Bulgur-Ratatoille, Gemüse, Nudeln oder Rosmarinkartoffeln.
							</p>	
							<ul>
									<li><h5>Lammbraten</h5><p> in Thymiansauce</p></li>
									<li><h5>Putenbraten </h5><p>in Currysauce</p></li>
									<li><h5>Putenröllchen</h5><p> gefüllt mit Spinat–Schafkäsemousse</p></li>
									<li><h5>Kalbsbraten</h5><p> in Weißweinsauce mit grünem Pfeffer</p></li>
									<li><h5>Rouladen</h5><p> von der Rinderhüfte gefüllt mit mediterranem Gemüse</p></li>
							</ul>
						</div>
				<h3 class="trigger3"><span class="pfeil" style="float:left;"></span>Fischgerichte</h3>
						<div class="card toggle_container3">
							<p class="p-content">
							Zu folgenden Fischgerichten empfehlen wir wahlweise als Beilage Wildreis, Gemüse, mit Spinat gefüllte überbackene Tomaten und Rosmarinkartoffeln.
							</p>	
							<ul>
									<li><h5>Lachs-Spinat-Torte</h5><p></p></li>
									<li><h5>Viktoriabarschfilets </h5><p>mit mediterranem Gemüse überbacken</p></li>
									<li><h5>Gebratener Lachs </h5><p>auf der Haut mit Safransauce</p></li>
							</ul>
						</div>
				<h3 class="trigger3"><span class="pfeil" style="float:left;"></span>Grillparty</h3>
						<div class="card toggle_container3">
							<ul>
								<li><h5>Tavuk Sis</h5><p>Hähnchenspieße</p></li>
								<li><h5>Siskebap</h5><p>Lammspieße</p></li>
								<li><h5>Hirtenspieße</h5><p>Rouladen vom Roastbeef gefüllt mit Schafkäse-Kräutermousse</p></li>
								<li><h5>Adana Kebap</h5><p>Hackfleischspieße vom Lamm und Rind</p></li>
								<li><h5>Suflaki</h5><p>Spieße vom Schweinerücken</p></li>
								<li><h5>Köfte</h5><p>Grillfrikadellen mit Lammfleisch</p></li>
								<li><h5>Döner Kebap </h5><p>mit Puten- und Hähnchenfleisch</p></li>
								<li><h5>Döner Kebap </h5><p>mit Lamm- und Kalbsfleisch</p></li>
								<li><h5>Gyros</h5><p>griechischer Drehspieß mit Schweinefleisch</p></li>
								<li><h5>Gemüsespieße</h5><p></p></li>
								<li><h5>Garnelenspieße</h5><p></p></li>								
							</ul>
						</div>
						


			</div>				
	    </article>
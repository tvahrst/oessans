		<article id="container_vorspeisen">
			<div class="gallery" data-type="video" data-offsetY="2400" data-speed="1">
					<?php
						$Bilder = array();
						$Ordner = 'images/vorspeisen';
						$dateiendungen = array('png', 'jpg');
						$anzahl = 40;
						$nummern = array();
						
						$ordner = opendir($Ordner);
						while ($Datei = readdir($ordner)) {
							if(!is_dir($Datei)) {
								if ($Datei != '..') {
									if (strstr($Datei, '.')) {
										$punkt = strrpos($Datei, '.');
										$endung = strtolower(substr($Datei, $punkt + 1));
										
										if (in_array($endung, $dateiendungen)) {
											$Bilder[] = $Ordner . '/' . $Datei;
										}
									} 
								}
							}
						}
						closedir($ordner);
						
						$anzahlbilder = count($Bilder) - 1;
						if ($anzahl > $anzahlbilder) {
							$anzahl = $anzahlbilder;
						}
						
						for ($i = 0; $i <= $anzahl; $i++) {
							srand(microtime()*1000000);
							$nummer = rand(0, $anzahlbilder);
							$path_parts = pathinfo($Bilder[$nummer]);
							if (!in_array($nummer, $nummern)) {
								$nummern[] = $nummer;
								echo '<a href="' . $Bilder[$nummer] . '" class="photobox" rel="external" title="' . $path_parts["filename"] . '"><div class="view"><img src="' . $Bilder[$nummer] . '" alt="' . $path_parts["filename"] . '" /><div class="mask"><h4>' . $path_parts["filename"] . '</h4></div></div></a>';
							} else {
								$i--;
							}
						}
						
					?>					
			</div>
				<h2 class="heading">vorspeisen</h2>
				<div class="content" >
					<h3 class="trigger2 trigger_active2"><span class="pfeil" style="float:left;"></span>Salate</h3>
					<div class="card toggle_container2" style="display: block;">
						<p class="p-content">
						Salate mit ausgewählten mediterranen Zutaten, wie Ziegenkäse, Meeresfrüchten oder Kichererbsen sind ein Erlebnis für den Gaumen. Probieren Sie in Ihrem Menü auch unbedingt unsere 
						türkischen Salate wie Kisir, mit Weizenschrot, Tomaten Petersilie und Pfefferminz.
						</p>
						<ul>
							<li><h5>Hirtensalat</h5> <p>mit Tomaten, Gurken, Paprika, roten Zwiebeln, Oliven, Schafkäse</p></li>
							<li><h5>Tomatensalat mit Ziegenkäse</h5><p></p></li>
							<li><h5>Meeresfrüchtesalat</h5><p></p></li>
							<li><h5>Kisir</h5><p>Salat mit Weizenschrot, Tomaten, Petersilie und Pfefferminz</p></li>
							<li><h5>Bunter Bohnensalat</h5><p>mit weißen, roten und grünen Bohnen</p></li>
							<li><h5>Auberginensalat</h5><p>Salat mit Auberginenmousse </p></li>
							<li><h5>Kichererbsensalat</h5><p>mit Rucola und Schafkäse</p></li>
							<li><h5>Blattsalate</h5><p>mit Nüssen und Parmesankäse</p></li>
							<li><h5>Linsensalat</h5><p>mit Rotbarbenfilets</p></li>
							<li><h5>Salate der Saison</h5><p>mit gebratenem Spargel und Entenbruststreifen</p></li>
							<li><h5>Mediterraner Nudelsalat</h5><p></p></li>
						</ul>
					</div>
					<h3 class="trigger2"><span class="pfeil" style="float:left;"></span>Suppen als Vorspeise oder Zwischengang</h3>
					<div class="card toggle_container2">
						<p class="p-content">
						Unsere Auswahl an Suppen bieten als Zwischengang oder Vorspeise eine perfekte Ergänzung an jedem Buffet. Ob eine vegetarische Rote-Linsen-Creme Suppe oder eine deftige Gyrossuppe, 
						unsere große Auswahl an Suppen bietet für jeden Geschmack die richtige Suppe
						</p>
						<ul>
							<li><h5>Rote Linsen-Creme-Suppe</h5><p></p></li>
							<li><h5>Sellerie-Creme-Suppe</h5><p></p></li>
							<li><h5>Zucchini-Creme-Suppe</h5><p></p></li>
							<li><h5>Almsuppe</h5><p>Joghurt-Reis-Suppe mit Pfefferminz</p></li>
							<li><h5>Fischsuppe</h5><p></p></li>
							<li><h5>Gyrossuppe</h5><p></p></li>
							<li><h5>Kürbissuppe</h5><p></p></li>
						</ul>
					</div>
				<h3 class="trigger2"><span class="pfeil" style="float:left;"></span>Kalte und warme Antipasti und Meze</h3>
					<div class="card toggle_container2">
						<p class="p-content">
						ist der perfekte Start in ein mediterran kulinarisches Erlebnis. Wir haben eine tolle Auswahl an Spezialitäten, wie Lammleber, Humus, Dolma oder Köfte – 
						vegetarisch oder mit Fleisch, kalt oder warm – 
						wir stellen für Sie passend eine Auswahl unserer außergewöhnlichen Spezialitäten zusammen.
						</p>
						<ul>
							
							<li><h5>Auberginenröllchen</h5><p>mit Tomatensauce, Käsemousse und Rucola</p></li>
							<li><h5>Cacik</h5><p>Joghurt-Quark-Creme mit Knoblauch</p></li>
							<li><h5>Havuc Ezme</h5><p>geraspelte geschmorte Möhren in Joghurt und Knoblauch</p></li>
							<li><h5>Humus</h5><p>Kichererbsenpüree mit Sesampaste</p></li>
							<li><h5>Zucchinicreme</h5><p> mit Joghurt, Knoblauch und Walnüssen</p></li>
							<li><h5>Lammleber aus der Pfanne</h5><p></p></li>
							<li><h5>Cerkez Tavugu</h5><p>Hühnerfleisch in Walnusssauce</p></li>
							<li><h5>Honigmelonen und frische Feigen mit Schinkenstreifen</h5><p></p></li>
							<li><h5>Gemüseplatte </h5><p>mit Auberginen, Paprikaschoten, Zucchini, Champignons und Schalotten</p></li>
							<li><h5>Gebratene Auberginen </h5><p>mit Paprikaschoten in Joghurt-Knoblauchsauce</p></li>
							<li><h5>Imam bayildi </h5><p>gefüllte Auberginen mit Zwiebeln und Tomaten</p></li>
							<li><h5>Potpourri</h5><p>von eingelegten getrockneten Tomaten, Schafkäse, Oliven und Peperoni</p></li>
							<li><h5>Mücver </h5><p>Zucchinipuffer</p></li>
							<li><h5>Meeresfrüchteplatte </h5><p>mit hausgebeiztem Lachs, Garnelen, Tintenfischen und Muscheln</p></li>
						</ul>
					</div>			
				</div>			
	    </article>
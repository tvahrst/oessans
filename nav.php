<header>
	<a href="/" title="Startseite"><img src="images/logo2.png" alt="Össan´s Partyservice Münster" id="logo"/></a>
	<div id="menu" class="menu">
		<ul>
			<li class="has-nosubmenu"><a href="/">Startseite</a></li>
			<li class="has-submenu"><a href="/">Partyservice</a>
				<ul class="sub-menu onnav">
					<li><a href="#feiern">Feste</a></li>
					<li><a href="#fingerfood">Fingerfood</a></li>	
					<li><a href="#vorspeisen">Vorspeisen</a></li>
					<li><a href="#hauptspeisen">Hauptgerichte</a></li>
					<li><a href="#desserts">Desserts</a></li>
					<li><a href="#contact">Kontakt</a></li>
				</ul>
			</li>
			<li><a href="philosophie.php">Philosophie</a></li>
			<li><a href="partyservice-karte.pdf" target="_blank">Speisekarte</a></li>
			<li><a href="wissenswertes-oessans-partyservice-muenster.php">FAQ</a></li>
			<li><a href="/#footer">Kontakt</a></li>
            <li><a href="impressum.php">Impressum</a></li>
            <li><a href="datenschutz.php">Datenschutz</a></li>
		</ul>
	</div>
	<a class="menu-link active" href="#menu"></a>
</header>
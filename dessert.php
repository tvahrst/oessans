		<article id="container_desserts">
			<div class="gallery" data-type="video" data-offsetY="1600" data-speed="3">
					<?php
						$Bilder = array();
						$Ordner = 'images/dessert';
						$dateiendungen = array('png', 'jpg');
						$anzahl = 40;
						$nummern = array();
						
						$ordner = opendir($Ordner);
						while ($Datei = readdir($ordner)) {
							if(!is_dir($Datei)) {
								if ($Datei != '..') {
									if (strstr($Datei, '.')) {
										$punkt = strrpos($Datei, '.');
										$endung = strtolower(substr($Datei, $punkt + 1));
										
										if (in_array($endung, $dateiendungen)) {
											$Bilder[] = $Ordner . '/' . $Datei;
										}
									} 
								}
							}
						}
						closedir($ordner);
						
						$anzahlbilder = count($Bilder) - 1;
						if ($anzahl > $anzahlbilder) {
							$anzahl = $anzahlbilder;
						}
						
						for ($i = 0; $i <= $anzahl; $i++) {
							srand(microtime()*1000000);
							$nummer = rand(0, $anzahlbilder);
							$path_parts = pathinfo($Bilder[$nummer]);
							if (!in_array($nummer, $nummern)) {
								$nummern[] = $nummer;
								echo '<a href="' . $Bilder[$nummer] . '" class="photobox" rel="bookmark" title="' . $path_parts["filename"] . '"><div class="view"><img src="' . $Bilder[$nummer] . '" alt="' . $path_parts["filename"] . '" /><div class="mask"><h4>' . $path_parts["filename"] . '</h4></div></div></a>';
							} else {
								$i--;
							}
						}
						
					?>						
			</div>
			<h2 class="heading">desserts</h2>
				<div class="content">
				<h3 class="trigger4 trigger_active4"><span class="pfeil" style="float:left;"></span>Süßes</h3>
						<div class="card toggle_container4" style="display: block;">
							<p class="p-content">
							Für viele ist das Dessert der wichtigste aller Gänge und auch in der türkischen Küche wird viel Wert auf die süßen Köstlichkeiten am Ende eines 
							Menüs gelegt. Ob Früchte, Baklava, Kadayif oder einfach eine köstliches Tirami-Su. Wir runden Ihr Menü perfekt für Sie ab.
							</p>
							<ul>
								<li><h5>Baklava</h5><p>verschiedene Sorten vom türkischen Süßgebäck</p></li>
								<li><h5>Kadayif</h5><p>Fadenteig, gefüllt mit Walnüssen</p></li>
								<li><h5>Getrocknete Feigen, Aprikosen und Datteln</h5><p>gefüllt mit Käse und Walnüssen</p></li>
								<li><h5>Ayva Tatlisi</h5><p>süß geschmorte Quitten mit Walnüssen an Mascarponecreme</p></li>
								<li><h5>Tiramisu</h5><p></p></li>
								<li><h5>Mousse au Chocolat</h5><p></p></li>
								<li><h5>Schwarzwälder Quarktorte</h5><p>Waldfrüchte mit Quark-Mascarpone-Creme</p></li>
								<li><h5>Kazandibi</h5><p>karamelisierter türkischer Pudding</p></li>
								<li><h5>Früchtecocktail</h5><p>mit Quark-Vanille-Sauce</p></li>
								<li><h5>Frische Feigen </h5><p>mit Ziegenkäse</p></li>
								<li><h5>Käseauswahl mit Früchten</h5><p></p></li>
							</ul>
				</div>				
	    </article>
	<?php
		require "header.php";
		require "script.php";
	?>
<meta name="description" content="Össan´s Partyservice Münster verwöhnt Ihre Gäste auf Hochzeiten, Firmenevents und privaten Feiern mit mediterranen Köstlichkeiten und türkischen Spezialitäten. In Münster und dem Münsterland.">
<title>Impressum Össan´s Partyservice in Münster</title>
<meta name="robots" content="noindex">
	</head>
	<body>
	<?php
		require "nav.php";
	?>
<section id="main" class="pearlon" style="z-index:0;" style="height:auto;">
	<article id="feiern" style="height:auto;">
		<section id="seventh" data-offsety="-100" data-speed="18" data-type="background" style="height:2000px;">    	
		<article id="impressum">
			<h2 class="heading">impressum</h2>
			<div class="content" style="left:100px;">
				<h5>Informationen lt. § 5 Telemediengesetz</h5>
					<ul id="address" style="left:0; position:relative; margin-bottom:20px; margin-top:10px !important;">
						<li>Betreiber der Website:</li>
						<li>Özhan Göcmen-Lütkenhöner</li>
						<li style="margin-top:14px;">Anschrift:</li>
						<li>Tönne-Vormann-Weg 42</li>
						<li>48167 Münster</li>
						<li style="margin-top:14px;">Kontaktdaten:</li>
						<li>Fon: 02506 - 6764</li>
						<li>Fax: 02506 - 3039396</li>
						<li>Mail: info@oessans.de</li>
						<li style="margin-top:14px;">Inhaltlich verantwortlich gemäß § 55 II RStV:</li>
						<li>Özhan Göcmen-Lütkenhöner</li>
						<li style="margin-top:14px;">Copyright</li>
						<li>© 2014 Özhan Göcmen-Lütkenhöner</li>
						<li>Alle Rechte sind vorbehalten. Das Kopieren, Vervielfältigen oder jegliche sonstige Verwendung von Bildern oder anderen Inhalten dieser Webseite ist ohne ausdrückliche Genehmigung nicht gestattet.</li>
						<li style="margin-top:14px;">Webdesign und Umsetzung: <a href="http://www.pilz-it.de" title="Pilz IT in Münster - Webdesign und Online Marketing" style="color:#fff;">Pilz IT - Webdesign und Online-Marketing Münster</a></li>
						<li style="margin-top:14px;">Fotos: <a href="http://www.adlinko.de" title="Werbung, Design, Marketing" style="color:#fff;">Monika Knohr</a></li>
					</ul>
						
				<h5>Haftungsausschluss</h5>

				<h5>1. Inhalt des Onlineangebotes</h5>
				<p>
				Der Webseitenbetreiber übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen.
				Haftungsansprüche gegen den Webseitenbetreiber, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Webseitenbetreiber kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt.
				Alle Angebote sind freibleibend und unverbindlich. Der Webseitenbetreiber behält es sich ausdrücklich vor, Teile der Seiten oder einzelne Angebote oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.
				</p>
				<h5>2. Verweise und Links</h5>
				<p>
				Bei direkten oder indirekten Verweisen auf fremde Internetseiten ("Links"), die außerhalb des Verantwortungsbereiches des Webseitenbetreiber liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem der Webseitenbetreiber von den Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern.
				Der Webseitenbetreiber erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der gelinkten/verknüpften Seiten hat der Webseitenbetreiber keinerlei Einfluss.
				Deshalb distanziert er sich hiermit ausdrücklich von allen Inhalten aller gelinkten/verknüpften Seiten, die nach der Linksetzung verändert wurden.
				Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise, sowie für Fremdeinträge in vom Webseitenbetreiber eingerichteten Firmenlisten, Gästebüchern, Diskussionsforen und Mailinglisten.
				Die Meinung von verlinkten Seiten kann, muss aber nicht unsere Meinung darstellen, selbst dann nicht, wenn diese allen Gesetzen und Vorschriften genüge tun und in keiner Form zu beanstanden sind.
				Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.
				</p>
				<h5>3. Urheber- und Kennzeichenrecht</h5>
				<p>
				Der Webseitenbetreiber ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.
				Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer.
				Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind!
				Das Copyright für veröffentlichte, vom Webseitenbetreiber selbst erstellte Objekte bleibt allein beim Webseitenbetreiber der Seiten.
				EineVervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Webseitenbetreiber nicht gestattet.
				Sollte sich auf den jeweiligen Seiten dennoch eine ungekennzeichnete, aber durch fremdes Copyright geschützte Grafik, ein Tondokument, eine Videosequenz oder Text befinden, so konnte das Copyright vom Webseitenbetreiber nicht festgestellt werden.
				Im Falle einer solchen, unbeabsichtigten Copyrightverletzung wird der Webseitenbetreiber das entsprechende Objekt nach Benachrichtigung aus seiner Publikation entfernen bzw. mit dem entsprechenden Copyright kenntlich machen.
				</p>
				<h5>4. Datenschutz</h5>
				<p>
				Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe persönlicher oder geschäftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis.
				Die Inanspruchnahme und Bezahlung aller angebotenen Dienste ist - soweit technisch möglich, gesetzlich erlaubt und zumutbar - auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines Pseudonyms gestattet.
				</p>
				<h5>5. Rechtswirksamkeit dieses Haftungsausschlusses</h5>
				<p>
				Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diesen Text verwiesen wurde.
				Sollten Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt. 
				</p>
			</div>			
	    </article>
	</article>
</section>

</body>
</html>
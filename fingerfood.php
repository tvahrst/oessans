		<article id="container_fingerfood">
			<div class="gallery" data-type="video" data-offsetY="1200" data-speed="1">
					<?php
						$Bilder = array();
						$Ordner = 'images/fingerfood';
						$dateiendungen = array('png', 'jpg');
						$anzahl = 40;
						$nummern = array();
						
						$ordner = opendir($Ordner);
						while ($Datei = readdir($ordner)) {
							if(!is_dir($Datei)) {
								if ($Datei != '..') {
									if (strstr($Datei, '.')) {
										$punkt = strrpos($Datei, '.');
										$endung = strtolower(substr($Datei, $punkt + 1));
										
										if (in_array($endung, $dateiendungen)) {
											$Bilder[] = $Ordner . '/' . $Datei;
										}
									} 
								}
							}
						}
						closedir($ordner);
						
						$anzahlbilder = count($Bilder) - 1;
						if ($anzahl > $anzahlbilder) {
							$anzahl = $anzahlbilder;
						}
						
						for ($i = 0; $i <= $anzahl; $i++) {
							srand(microtime()*1000000);
							$nummer = rand(0, $anzahlbilder);
							$path_parts = pathinfo($Bilder[$nummer]);
							if (!in_array($nummer, $nummern)) {
								$nummern[] = $nummer;
								echo '<a href="' . $Bilder[$nummer] . '" class="photobox" rel="search" title="' . $path_parts["filename"] . '"><div class="view"><img src="' . $Bilder[$nummer] . '" alt="' . $path_parts["filename"] . '" /><div class="mask"><h4>' . $path_parts["filename"] . '</h4></div></div></a>';
							} else {
								$i--;
							}
						}
						
					?>						
			</div>
			<h2 class="heading">fingerfood</h2>
			<div class="content">
				<h3 class="trigger trigger_active"><span class="pfeil" style="float:left;"></span>Perfekt für Empfänge oder die Gartenparty</h3>
						<div class="card toggle_container" style="display: block;">
							<p class="p-content">
							Für einen repräsentativen Empfang auf Hochzeiten oder sonstigen feierlichen Anlässen ist Fingerfood der beste Begleiter für ein kommunikatives Zusammenkommen.
							Die mundgerechten Snacks sind sehr gut geeignet für Parties und Empfänge ohne große Sitzmöglichkeiten. Auf Wunsch bieten wir Ihnen gerne auch weitere Services an, 
							damit Sie sich an Ihrem großen Tag keine Sorgen um den perfekten Ablauf machen müssen. Geschirr, Besteck, begleitende Weine, freundliches Servicepersonal oder weitergehende Unterstützung bei der Planung und Organisation Ihres Events - wir erarbeiten mit Ihnen gemeinsam ein Konzept.
							Ich verstehe es als meinen Auftrag Ihnen das bestmögliche kulinarische Erlebnis zu bieten. Tauchen Sie ab in die köstliche Welt der mediterranen Küche und lassen Sie sich verwöhnen.
							Von türkischen Klassikern wie Börek, Köfte und Dolma, über vegetarische Spezialitäten aus dem Mittelmeerraum wie Meeresfrüchtesalat oder Falafel-Wraps bis hin zu delikaten Canapés, z.B. mit hausgebeiztem Lachs und Forellencaviar oder saftigem Roastbeef 
							haben wir eine sehr breite Palette an Fingerfood in unserem Angebot. Lassen Sie sich inspirieren! Für eine vollständige Übersicht aller Speisen, habe ich Ihnen die komplette Speisekarte im Menü zum Download bereit gestellt.
							</p>
							<ul>
								<li><h5>Köfte</h5><p>Hackbällchen vom Lamm und Rind</p></li>
								<li><h5>Sigara Börek</h5><p>gefüllte Teigröllchen mit Schafkäse</p></li>
								<li><h5>Avcibörek</h5><p>gefüllte Teigröllchen mit Hackfleisch</p></li>
								<li><h5>Dolma</h5><p>gefüllte Weinblätter mit Reis-Kräuter-Mischung</p></li>
								<li><h5>Chicoree-Schiffchen</h5><p>mit Eier-Thunfischpaste</p></li>
								<li><h5>Gemüse-Wrap</h5><p>mit Falafel</p></li>
								<li><h5>Roastbeefröllchen</h5><p>gefüllt mit Kräuter-Gemüsemousse</p></li>
								<li><h5>Pfannkuchen-Rollen</h5><p>mit Carpaccio vom Rind und Austernpilzen</p><p>mit Räucherlachs und Rucola</p><p>mit Humus, Pinienkernen und Granatapfel</p><p>mit Zucchini und Champignons</p></li>
								<li><h5>Fisch- und Garnelenspieße</h5><p></p></li>
								<li><h5>Gemüsespieße</h5><p></p></li>
								<li><h5>Tomaten-Mozzarella-Spieße</h5><p></p></li>
								<li><h5>Ofen- Tomaten gefüllt</h5><p>mit Käse-Kräutermousse</p></li>
								<li><h5>Mini-Pizza</h5><p>z.B. Lahmacun, Spinat mit Knoblauchwurst, oder vegetarisch</p></li>
						</ul>
						</div>					
						<h3 class="trigger"><span class="pfeil" style="float:left;"></span>Canapés</h3>
						<div class="card toggle_container">
							<ul>
								<li><h5>Räucherlachs</h5><p></p></li>
								<li><h5>Garnelen</h5><p></p></li>
								<li><h5>Forellenkaviar</h5><p></p></li>
								<li><h5>Forellencreme</h5><p></p></li>
								<li><h5>Sardellen- und Olivenpaste</h5><p></p></li>
								<li><h5>Käsecreme und Walnuß</h5><p></p></li>
								<li><h5>Ziegenkäse</h5><p></p></li>
								<li><h5>Gemüsepaste</h5><p></p></li>
								<li><h5>Entenleberpaste</h5><p></p></li>
								<li><h5>Roastbeef</h5><p></p></li>
						</ul>
						</div>			
			</div>			
	    </article>